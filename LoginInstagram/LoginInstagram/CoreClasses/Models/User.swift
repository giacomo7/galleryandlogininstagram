//
//  User.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    
    var accessToken:String
    
    var name :String
    
    private init(accessToken:String = EMPTY_STRING, name : String = EMPTY_STRING) {
        
        self.accessToken = accessToken
        
        self.name = name
        
    }
    
    required convenience init?(coder decoder: NSCoder) {
        
        let accessToken = decoder.decodeObject(forKey: "accessToken") as! String
        
        let name = decoder.decodeObject(forKey: "name") as! String
        
        self.init(accessToken:accessToken, name : name)
    }
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.accessToken , forKey: "accessToken")
        
         aCoder.encode(self.accessToken , forKey: "name")
        
    }
    
    class func clearUser() {
        
        UserDefaults.standard.removeObject(forKey: "User")
        
        UserDefaults.standard.synchronize()
    }
    
    func saveUser() {
        
        let userDefault = UserDefaults.standard
        
        let data : NSData = NSKeyedArchiver.archivedData(withRootObject: self) as NSData
        
        userDefault.set(data, forKey: "User")
    }
    
    class func loadUser() -> User? {
        
        var user:User?
        
        let userDefault = UserDefaults.standard
        
        if let userData = userDefault.object(forKey: "User") as? NSData {
            
            let decodeData = NSKeyedUnarchiver.unarchiveObject(with: userData as Data)
            
            user = decodeData as? User
        }
        else {
            
            user = User.init()
        }
        
        return user
    }
    
   
}
