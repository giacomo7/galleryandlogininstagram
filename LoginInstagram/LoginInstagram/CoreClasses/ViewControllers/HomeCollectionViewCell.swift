//
//  HomeCollectionViewCell.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        
    }

    override func prepareForReuse() {
        
    }
    
    func populateData(url : String){
        
        self.activityIndicator.startAnimating()
        
        let url = URL(string: url)
        
        self.imgView.sd_setImage(with: url, placeholderImage: PROFILE_PLACEHOLDER, options:[] ,completed: { (img, _, _, _) in
            
            if (img != nil) {
                
                DispatchQueue.main.async {
                    
                    self.imgView.image = img
                    
                }
                
                
            }
            self.activityIndicator.stopAnimating()
        })

    }
}
