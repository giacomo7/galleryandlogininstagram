//
//  LoginWebViewController.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import Alamofire

let URL_FORMAT = "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True"

class LoginWebViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Utility.transparentNavBar(controller: self.navigationController!)

        self.addLeftNavigationButtonWithTitle(title: "X")
        
        self.setDefault()
    }

    override func leftNavigationButtonPressed() {
        self.dismiss(animated: true) {
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setDefault(){
     
        
        self.webView.delegate = self
        
        let authURL = String(format: URL_FORMAT , arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
       
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        
        self.webView.loadRequest(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        print(requestURLString)
    
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            
            let accessToken = requestURLString.substring(from: range.upperBound)
            
            self.dismiss(animated: true, completion: { 
                
                let user = User.loadUser()
                
                user?.accessToken = accessToken
                
                user?.saveUser()
                
                APP_DELEGATE.setAppState()
            })
            
            
            
            return false
        }
        return true
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
            return checkRequestForCallbackURL(request: request)
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        self.showActivityIndicator()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.hideActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
    
  
}
