//
//  HomeViewController.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import Alamofire


class HomeViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {

    @IBOutlet weak var cv: UICollectionView!
    
    var userDictionary : [String : AnyObject]?
    
    var imagesUrl = Array<String>()
    
    var standardImageUrl = Array<String>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Utility.transparentNavBar(controller: self.navigationController!)
        
        self.setDefault()
        
        self.addLeftNavigationButtonWithImage(imageNamed: "arrow")

        self.addRightNavigationButtonWithImage(imageNamed: "icon_logout")
    }

    
    override func rightNavigationButtonPressed() {
        
        super.rightNavigationButtonPressed()
        
     
        let alert = UIAlertController(title:"Logout", message:"Are you sure you want to log out?", preferredStyle:.alert)
        
        let ok = UIAlertAction(title:"OK", style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            

            User.clearUser()
            
            
            APP_DELEGATE.setAppState()
        })
        
        let cancel = UIAlertAction(title:"Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in })
        
        alert.addAction(ok)
        
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - helperMethod
    func setDefault(){
        
        self.cv.isHidden = true
        
        self.navigationItem.title = ""
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.callServiceInstagramMedia()
        
//
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowImageViewController"{
            
            let vc = segue.destination as! UINavigationController
            
            if let controller = vc.viewControllers.first as? ShowImageViewController{
                
                if let value = sender as? String{
                    
                    controller.url = value
                }
            }
            
            
        
        }
    }

    
    func callServiceInstagramMedia(){
        
        
        let user = User.loadUser()
        
        let url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + (user?.accessToken)!
      
        
        
        self.showActivityIndicator()
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
        
            
            
            if response.result.isSuccess{
        
                self.hideActivityIndicator()
                
                if let value = response.result.value {
        
                    let data = value as? [String : AnyObject]
                    
                    self.extractResponse(dict: data!)
                }
            }else{
                
                self.hideActivityIndicator()
                
                self.showErrorAlertWithMessage(message: "Server Error")
            }
                
        }
    }
    
    func extractResponse(dict : [String : AnyObject]){
        
        print(dict)
        
        if let meta = dict["meta"]{
            
            if let code = meta["code"] as? Int{
                
                if code == 200 {
                    
                    let data = dict["data"] as? [[String : AnyObject]]
                    
                    
                    for singleDict in data!{
                        
                        if let thumbnailImage = singleDict["images"]{
                            
                            if let thumbnail = thumbnailImage["thumbnail"] as? [String : AnyObject]{
                                
                                if let url = thumbnail["url"] as? String{
                                    
                                    self.imagesUrl.append(url)
                                }
                            }
                            if let thumbnail = thumbnailImage["standard_resolution"] as? [String : AnyObject]{
                                
                                if let url = thumbnail["url"] as? String{
                                    
                                    self.standardImageUrl.append(url)
                                }
                            }
                        }
                        
                    }
                    self.cv.isHidden = false
                    
                    if let name = self.userDictionary?["full_name"] as? String{
                        
                        self.navigationItem.title = name
                    }
                    
                    self.cv.reloadData()
                }
                
                else{
                    
                    self.showErrorAlertWithMessage(message: "Could not get Data from server")
                }

            }
            else{
                
                self.showErrorAlertWithMessage(message: "Could not get Data from server")
            }
        }
        else{
            
            self.showErrorAlertWithMessage(message: "Could not get Data from server")
        }
 
    }
    
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.imagesUrl.count == 0{
            
            return 0
        }
        else{
            
            return self.imagesUrl.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : HomeCollectionViewCell = self.cv.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
        
        let url = self.imagesUrl[indexPath.row]
        
        cell.populateData(url: url)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "header",for: indexPath) as! HomeCollectionReusableView
        
           headerView.populateHeader(dict: self.userDictionary)
            
            return headerView
            
        default:
            
            assert(false, "Unexpected element kind")
        }
        
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = 0.3125 * self.view.bounds.width
        
        let height = 0.1984126984 * self.view.bounds.height
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let height = 0.4861111111 * self.view.bounds.height
        
        return CGSize(width: self.view.bounds.width, height: height)
    }
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let url = self.standardImageUrl[indexPath.row]
    
        self.performSegue(withIdentifier: "ShowImageViewController", sender: url)
    }
}
