//
//  ShowImageViewController.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import Alamofire

class ShowImageViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    var url : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setDefault()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - helperMethod
    
    func setDefault(){
        
        let button = UIBarButtonItem(title: "X", style:UIBarButtonItemStyle.done, target: self, action: Selector(NAV_BAR_LEFT_BTN_PRESSED))
        
        self.navigationItem.setLeftBarButton(button, animated: true)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        
        if let tempUrl = self.url{
            
            self.activityIndicator.startAnimating()
            
            let url = URL(string: tempUrl)
            
            self.imgView.sd_setImage(with: url, placeholderImage: PROFILE_PLACEHOLDER, options:[] ,completed: { (img, _, _, _) in
                
                if (img != nil) {
                    
                    DispatchQueue.main.async {
                        
                        self.imgView.image = img
                        
                    }
                    
                    
                }
                self.activityIndicator.stopAnimating()
            })

        }
        else{
            
            self.activityIndicator.stopAnimating()
            
            self.imgView.isHidden = true
            
           // self.showErrorAlertWithMessage(message: "Could not load image")
        }
        
    }
    
     func leftNavigationButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    

}
