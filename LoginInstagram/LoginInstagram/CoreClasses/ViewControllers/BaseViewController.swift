//
//  BaseViewController.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import WSProgressHUD
import TSMessages

let KING_GREEN_DARK_COLOR = Utility.colorWithRGB(red: 68, green:136, blue: 30)

let KING_GREEN_LIGHT_COLOR = Utility.colorWithRGB(red: 109, green: 163, blue: 79)

let NAV_BAR_BAR_TINT_COLOR = Utility.colorWithRGB(red: 244, green:246, blue: 252)

let NAV_BAR_TINT_COLOR = UIColor.black//Utility.colorWithRGB(red: 255, green:255, blue: 255) // white

let NAV_BAR_FONT_SIZE = CGFloat(16.0)

let NAV_BAR_RIGHT_BTN_PRESSED = "rightNavigationButtonPressed"

let NAV_BAR_LEFT_BTN_PRESSED = "leftNavigationButtonPressed"

let INTER_POP_GES_RECOGNIZER = "interactivePopGestureRecognizer"

class BaseViewController: UIViewController {
    
    //    var activityView = MBProgressHUD()
    
    var hud:WSProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setDefaultsBase()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        self.hideActivityIndicator()
    }
    
    func setDefaultsBase(){
        
        self.setDefaultsForActivityView()
        
        self.setDefaultsForNavigation()
    }
    
    func setDefaultsForNavigation(){
        
        self.navigationController?.navigationBar.barTintColor = NAV_BAR_BAR_TINT_COLOR
//
//        self.navigationController?.navigationBar.tintColor = NAV_BAR_TINT_COLOR
        
        //self.navigationController?.navigationBar.tintColor = UIColor.clearColor()
        
        let navigationFont = UIFont(name: "AvenirNext-DemiBold", size: NAV_BAR_FONT_SIZE)!
        
        let navigationForegroundColor = NAV_BAR_TINT_COLOR
        
        let navigationAttributes:Dictionary = [NSFontAttributeName:navigationFont,NSForegroundColorAttributeName:navigationForegroundColor]
        
        UINavigationBar.appearance().titleTextAttributes = navigationAttributes
        
        self.navigationController?.navigationBar.titleTextAttributes = navigationAttributes
    }
    
    func setDefaultsForActivityView(){
        
        let navigationController:(UINavigationController)? = self.navigationController
        
        hud = WSProgressHUD(frame: UIScreen.main.bounds)
        hud.setProgressHUDIndicatorStyle(.mmSpinner)
        
        if (navigationController != nil) {
            
            self.navigationController?.view.addSubview(hud)
        }
        else{
            
            self.view.addSubview(hud)
        }
        
        //        activityView.isUserInteractionEnabled = true
        
        //        var spinnerColor = UIColor()
        //
        //        spinnerColor = UIColor.white
        //
        //        let spinner = RTSpinKitView(style: RTSpinKitViewStyle.styleWave, color: spinnerColor)
        
        //        activityView.mode = MBProgressHUDMode.customView
        //
        //        activityView.customView = spinner
        //
        //        activityView.isSquare = true;
        //
        //        activityView.contentColor = UIColor.black
        //
        //        activityView.autoCenterInSuperview()
        //
        //        activityView.bezelView.color = UIColor.black
        
    }
    
    func addRightNavigationButtonWithImage(imageNamed:String){
        
        let image = UIImage(named: imageNamed)
        
        let button = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: Selector(NAV_BAR_RIGHT_BTN_PRESSED))
        
        self.navigationItem.setRightBarButton(button, animated: true)
    }
    
    func addLeftNavigationButtonWithImage(imageNamed:String){
        
        let image = UIImage(named: imageNamed)
        
        let button = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: Selector(NAV_BAR_LEFT_BTN_PRESSED))
        
        self.navigationItem.setLeftBarButton(button, animated: true)
    }
    
    func addRightNavigationButtonWithTitle(title:String){
        
        let button = UIBarButtonItem(title: title, style:UIBarButtonItemStyle.done, target: self, action: Selector(NAV_BAR_RIGHT_BTN_PRESSED))
        
        self.navigationItem.setRightBarButton(button, animated: true)
    }
    
    func addLeftNavigationButtonWithTitle(title:String){
        
        let button = UIBarButtonItem(title: title, style:UIBarButtonItemStyle.done, target: self, action: Selector(NAV_BAR_LEFT_BTN_PRESSED))
        
        self.navigationItem.setLeftBarButton(button, animated: true)
    }
    
    func leftNavigationButtonPressed(){
        
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func rightNavigationButtonPressed(){
        
        // override to add functionality
    }
    
    func showActivityIndicator(){
        
        if let _ = hud{
            
            hud.show(with: .clear)
        }
        
        
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        //
        //        activityView.show(animated: true)
        
        let navigationController = self.navigationController
        
        let canHavePopGesture = navigationController?.responds(to:Selector(INTER_POP_GES_RECOGNIZER))
        
        if canHavePopGesture != nil{
            
            self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        }
    }
    
    func showActivityIndicator(loadingMessage:String){
        
        
        
        //        activityView.label.text = loadingMessage
        
        self.showActivityIndicator()
    }
    
    func hideActivityIndicator(){
        
        if let _ = hud{
            hud.dismiss()
        }
        
        
        //        UIApplication.shared.endIgnoringInteractionEvents()
        
        
        //        activityView.label.text = EMPTY_STRING
        //
        //        activityView.hide(animated : true)
        
        let navigationController = self.navigationController
        
        let canHavePopGesture = navigationController?.responds(to:Selector(INTER_POP_GES_RECOGNIZER))
        
        if (canHavePopGesture != nil){
            
            self.navigationController?.interactivePopGestureRecognizer!.isEnabled = true
        }
    }
    
    func showErrorAlertWithMessage(message:String!) {
        
        
        // let manager = LanguageManager.sharedLanguageManager()
        
        //  let msg = manager.getTranslationForKey(message)
        
        let msg = message
        
        if msg != nil {
            
            Utility.showAlertInController(controller: self, title: kAlertViewErrorTitle, subTitle: msg!, type: TSMessageNotificationType.error, callback: {})
        }
        else {
            
            Utility.showAlertInController(controller: self, title: kAlertViewErrorTitle, subTitle: message!, type: TSMessageNotificationType.error, callback: {})
        }
    }
}
