//
//  Utility.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import TSMessages


let K_EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"

class Phone : UIView{
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_7P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    }
}

class Utility: NSObject {
    
    // validations
    
    class func isEmailValid(emailString:NSString,strictFilter:Bool) -> Bool {
        
        let stricterFilterString:NSString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let laxString:NSString = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*"
        
        var emailRegex:NSString! = nil
        
        if(strictFilter){
            
            emailRegex = stricterFilterString
        }
        else{
            
            emailRegex = laxString
        }
        
        let emailTest:NSPredicate = NSPredicate(format:"SELF MATCHES %@",emailRegex)
        
        return emailTest.evaluate(with: emailString)
    }
    
    class func isStringOnlyContainsAlphaNumericAndUnderscore(candidate: String) -> Bool{
        
        let regex = "^[a-zA-Z0-9_]*$"
        
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: candidate)
    }
    
    class func validateEmail(candidate: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{3,6}"
        
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    class func validateURL(candidate: String) -> Bool {
        
        let regex = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: candidate)
    }

    // font
    
    class func updateFontSizeAccordingToDevice(font:UIFont) -> UIFont {
        
        let currentFontSize = font.pointSize
        
        if Phone.DeviceType.IS_IPHONE_4_OR_LESS || Phone.DeviceType.IS_IPHONE_5 {
            
            return font.withSize(currentFontSize) // unchanged as we develop for 5s
        }
        else if Phone.DeviceType.IS_IPHONE_6 || Phone.DeviceType.IS_IPHONE_7 {
            
            return font.withSize(currentFontSize + 1.0)
        }
        else if Phone.DeviceType.IS_IPHONE_6P || Phone.DeviceType.IS_IPHONE_7P {
            
            return font.withSize(currentFontSize + 1.5)
        }
        else if Phone.DeviceType.IS_IPAD {
            
            return font.withSize(currentFontSize + 6)
        }

        return font
    }
    
    // utility
    
    class func suitableHeightForViewWithText(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label:UILabel = UILabel(frame:CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        
        label.numberOfLines = 0
        
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        label.font = font
        
        label.text = text
        
        label.sizeToFit()
        
        return label.frame.height
    }
    
    class func callNumber(phoneNumber:String) {
        
        if let phoneCallURL:NSURL = NSURL(string:"tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            
            if (application.canOpenURL(phoneCallURL as URL)) {
                
                application.openURL(phoneCallURL as URL);
            }
        }
    }
    
    class func universalUniqueId() -> String{
        
        let uuid = NSUUID().uuidString
        
        return uuid
    }
    
    class func LimitNumberOfCharacterInTxtField(txtField : UITextField, replacementString : String, count : Int) -> Bool{
    
        var result = true
        
        let tempString : String!
        
        tempString = txtField.text
        
        if replacementString == ""{
            
            result = true
        }
        else{
            
            result = tempString.characters.count < count
        }
        
        return result
    }
    
    class func txtFieldPunctuationCharacterCheck(txtField : UITextField, replacementString : String) -> Bool{
        
        var result = false
        
        let characterSetNotAllowed = NSCharacterSet.punctuationCharacters
        
        if let _ = replacementString.rangeOfCharacter(from: characterSetNotAllowed){
            
            result = false
        }
        else{
            result = true
        }
        return result
    }
    
    class func lettersOnly(string : String) -> Bool{
        
        var result = true
        
        let charactersAllow = NSCharacterSet.letters
        
        if string == ""{
            
            result = true
        }
        else{
        
            if string.rangeOfCharacter(from: charactersAllow) == nil{
                
                result = false
            }
        }
      return result
    }
    
    class func colorWithRGB(red : CGFloat, green : CGFloat, blue : CGFloat) -> UIColor{
        
        let color : UIColor = UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: 1.0)
        
        return color
    }
    
    class func unixTimeStampToDateAndTime(time : String) -> String{
        
        let timeStamp : TimeInterval = Double(time)!
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        
        dayTimePeriodFormatter.dateFormat = "E, d MMM h:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        print(dateString)
        
        return dateString
    }
    
    class func convertNumberOrStringToString(value : AnyObject?) -> String{
        
        if value == nil {
            
           return EMPTY_STRING
        }
        
        if let temp = value as? NSNumber{
            
            return String(describing: temp)
        }
        else if let temp = value as? String{
            
            return temp
        }
        else{
            
            return EMPTY_STRING
        }
    }
    
    class func makingSubTitleFromTagString(tags : [String])-> String{
        
        var subTitle = ""
        
        for tag in tags{
            
            subTitle.append(tag)
            subTitle.append(",")
        }
        return subTitle
    }
    
    class func exponentize(str: String) -> String {
        
        let supers = [
            "1": "\u{00B9}",
            "2": "\u{00B2}",
            "3": "\u{00B3}",
            "4": "\u{2074}",
            "5": "\u{2075}",
            "6": "\u{2076}",
            "7": "\u{2077}",
            "8": "\u{2078}",
            "9": "\u{2079}"]
        
        var newStr = ""
        var isExp = false
        for (_, char) in str.characters.enumerated() {
            if char == "^" {
                isExp = true
            } else {
                if isExp {
                    let key = String(char)
                    if supers.keys.contains(key) {
                        newStr.append(Character(supers[key]!))
                    } else {
                        isExp = false
                        newStr.append(char)
                    }
                } else {
                    newStr.append(char)
                }
            }
        }
        return newStr
    }
    
    // nav bar
    
    class func transparentNavBar (controller:UINavigationController) {
        
        controller.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        
        controller.navigationBar.shadowImage = UIImage()
        
        controller.navigationBar.isTranslucent = false
        
//        controller.view.backgroundColor = .clear

//        controller.navigationBar.backgroundColor = .red
        

    }
    
    class func hideTransparentNavBar (controller:UINavigationController) {
        
        controller.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        
        controller.navigationBar.shadowImage = nil
        
        controller.navigationBar.isTranslucent = false
    }
    
    // image
    
    class func areEqualImages(img1: UIImage, img2: UIImage) -> Bool {
        
        guard let data1:NSData = UIImagePNGRepresentation(img1) as NSData? else { return false }
       
        guard let data2:NSData = UIImagePNGRepresentation(img2) as NSData? else { return false }
        
        return data1.isEqual(to: data2 as Data)
    }
    
    class func imageWithRectAndColor(rect:CGRect,color:UIColor) -> UIImage{
        
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        
        context!.fill(rect);
        
        let img:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        
        UIGraphicsEndImageContext()
        
        return img
    }
    
    // dates
    
    class func numberOfYearsBetweenDates(date1:NSDate!,date2:NSDate!) -> Int{
        
        let calendar:NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
        
        let component = NSCalendar.Unit.year
        
        let datesComponents:DateComponents = calendar.components(component, from:date1 as Date, to: date2 as Date, options:NSCalendar.Options.matchStrictly)
        
        return datesComponents.year!
    }
    
    class func monthDayYearDateStringFromDate(date:Date) -> String {
        
        let dateFormat:DateFormatter = DateFormatter()
        
        dateFormat.dateFormat = "MM.dd.yyyy"
        
        return dateFormat.string(from: date)
    }
    
    class func timeStringFromDate(date:Date) -> String {
        
        let dateFormat:DateFormatter = DateFormatter()
        
        dateFormat.dateFormat = "hh:mm a"
        
        return dateFormat.string(from: date)
    }
    
    class func oxknockFormatDateStringFromDate(date:Date) -> String {
        
        let dateFormat:DateFormatter = DateFormatter()
        
        dateFormat.dateFormat = "MMM dd, yyyy hh:mm a" // e.g Wed, Dec 14 2011 1:50 PM
        
        return dateFormat.string(from: date)
    }
    
    // alerts
    
    class func showAlertInController(controller : UIViewController , title : String!, subTitle: String, type : TSMessageNotificationType, callback: (() -> Void)){
        
        TSMessage.showNotification(in: controller, title: title, subtitle: subTitle, image: nil, type: type, duration: 3.0, callback: {
            
        }, buttonTitle: nil, buttonCallback: nil, at: TSMessageNotificationPosition.top, canBeDismissedByUser: true)
        
    }
    
    class func monthNumberFrom(month : String) -> Int{
        
        var number : Int!
        
        if month == "Jan"{
            
            number = 1
        }
        else if month == "Feb"{
            
            number = 2
        }
        else if month == "Mar"{
            
            number = 3
        }
        else if month == "Apr"{
            
            number = 4
        }
        else if month == "May"{
            
            number = 5
        }
        else if month == "Jun"{
            
            number = 6
        }
        else if  month == "Jul"{
            
            number = 7
        }
        else if month == "Aug"{
            
            number = 8
        }
        else if month == "Sep"{
            
            number = 9
        }
        else if month == "Oct"{
            
            number = 10
        }
        else if month == "Nov"{
            
            number = 11
        }
        else if month == "Dec"{
            
            number = 12
        }
        return number
    }
    
    class func stringMonthFrom(monthNumber : Int)-> String?{
        
        var month : String = ""
        
        if monthNumber == 1{
            
            month = "Jan"
        }
        else if monthNumber == 2{
            
            month = "Feb"
        }
        else if monthNumber == 3{
            
            month = "Mar"
        }
        else if monthNumber == 4{
            
            month = "Apr"
        }
        else if monthNumber == 5{
            
            month = "May"
        }
        else if monthNumber == 6{
            
            month = "Jun"
        }
        else if monthNumber == 7{
            
            month = "Jul"
        }
        else if monthNumber == 8{
            
            month = "Aug"
        }
        else if monthNumber == 9{
            
            month = "Sep"
        }
        else if monthNumber == 10{
            
            month = "Oct"
        }
        else if monthNumber == 11{
            
            month = "Nov"
        }
        else if monthNumber == 12{
            
            month = "Dec"
        }
        return month
    }
    
   
}
