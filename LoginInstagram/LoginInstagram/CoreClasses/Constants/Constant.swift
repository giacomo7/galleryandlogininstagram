//
//  Constant.swift
//
//  Created by GiaBoemio on 25/01/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit

let APP_DELEGATE = (UIApplication.shared.delegate as! AppDelegate)

let HOME_STORY_BOARD = UIStoryboard(name: "Home", bundle: Bundle.main)

let MAIN_STORY_BOARD = UIStoryboard(name: "Main", bundle: Bundle.main)

let PROFILE_PLACEHOLDER = UIImage(named:"icon_profile_round")


// user defaults
let APP_KEY    = ""
let CLIENT_KEY = ""

let RED_TEXT_COLOR  = "#5e0000"
let GRAY_TEXT_COLOR = "#929292"
let APP_RED_COLOR   = "#9E1710"

//Alert Titles

let kAlertViewErrorTitle   = "Something went wrong!"
let kAlertViewSuccessTitle = "Success"
let kAlertViewInfoTitle    = "Info!"

//

let TRUE_STR = "1"
let FALSE_STR = "0"

// keys

let kAPI_KEY = ""

let kDEVICE_TYPE = "iOS"

let kDEVICE_TOKEN = "deviceToken"

let kMeta = "meta"

let kResult = "result"

let kErrorCode = "code"

let kMessage = "message"

//

let EMPTY_STRING = ""

let UNKNOWN_ERROR = "Unknown error!"

let NOT_AVAILABLE = "N/A"


